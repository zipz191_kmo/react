import { View, Image, Text } from 'react-native';
import { styles } from '../styles/common';
import Search from './Search';
import { styles as pageStyles } from '../styles/home';

export const Header = ({ searchData }) => {
  return (
    <View style={[pageStyles.header, styles.pageContent]}>
      <Image style={[styles.block, pageStyles.logo]} source={require('../../../assets/images/logo.png')} />
      <View>
        <Text style={[styles.title, styles.textWhite, styles.block]}>Let's cook!</Text>
        <Search searchData={searchData} />
      </View>
    </View>
  );
}
