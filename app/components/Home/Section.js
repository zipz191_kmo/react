import { View, Text } from "react-native"
import { styles } from "../styles/common"

export const Section = ({ title, children }) => {
  return (
    <View style={styles.section}>
      <Text style={[styles.title, styles.block]}>{title}</Text>
      {children}
    </View>
  )
}
