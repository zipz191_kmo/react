import React, { useState } from 'react';
import { View, Text, TextInput, FlatList } from 'react-native';
import { useRouter } from '../router';
import { styles } from '../styles/common';
import { styles as pageStyles} from '../styles/home';

const Search = ({ searchData }) => {
  const router = useRouter();
  const [searchQuery, setSearchQuery] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  const handleSearchTextChange = (text) => {
    setSearchQuery(text);
    if (text.length >= 3) {
      const filteredRecipes = searchData.filter((recipe) =>
        recipe.title.toLowerCase().includes(text.toLowerCase())
      );
      setSuggestions(filteredRecipes.slice(0, 3));
    } else {
      setSuggestions([]);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={[pageStyles.searchBar, suggestions.length && pageStyles.searchBarExpanded]}
        placeholder="Search for a recipe"
        value={searchQuery}
        onChangeText={handleSearchTextChange}
        clearButtonMode="while-editing"
        autoCorrect={false}
        autoFocus={true}
      />
      <View style={pageStyles.searchSuggestions}>
        <FlatList
          data={suggestions}
          keyExtractor={(item, index) => `${item.title}-${index}`}
          renderItem={({ item }) => <Text onPress={() => router.toRecipeDetails(item.id)} style={pageStyles.searchSuggestion}>{item.title}</Text>}
        />
      </View>
    </View>
  );
}

export default Search
