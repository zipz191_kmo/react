import React from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { styles as pageStyles } from '../styles/home';
import { styles } from '../styles/common'
import { useRouter } from '../router';

const TrendingCarousel = ({ recipes }) => {
  const router = useRouter();
  return (
    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
      {recipes.map((recipe, index) => (
        <TouchableOpacity onPress={() => router.toRecipeDetails(recipe.id)}>
          <View key={index} style={pageStyles.carouselTile}>
            <Image source={recipe.image} style={pageStyles.carouselTileImage} />
            <View style={[pageStyles.carouselTileTitleContainer, pageStyles.tileInfo]}>
              <Text style={[styles.textWhite, pageStyles.carouselTileTitle]}>{recipe.title}</Text>
            </View>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
};

export default TrendingCarousel;
