import { View } from 'react-native'
import RecipeTile from './Tile'
import TrendingCarousel from './TrendingCarousel'
import { recipes } from '../../../data.json';
import { Texts as T } from '../texts';
import { Section } from './Section';
import { styles } from '../styles/common';
import { Header } from './Header';

export const Home = () => {
  return (
    <View>
      <Header searchData={getSearchData()} />
      <View style={styles.pageContent}>
        <Section title={T.TrendingHeader}>
          <TrendingCarousel recipes={getTrending()} />
        </Section>
        <Section title={T.SuggestionHeader}>
          <RecipeTile recipe={recipes[getRandomRecipeIndex()]} />
        </Section>
      </View>
    </View>)
}

const getTrending = () => {
  let numbers = [];

  while (numbers.length < 10) {
    const num = getRandomRecipeIndex();
    if (!numbers.includes(num)) {
      numbers.push(num);
    }
  }

  let trending = [];
  for (const number of numbers) {
    trending.push(recipes[number]);
  }

  return trending;
}

const getRandomRecipeIndex = () => {
  return Math.floor(Math.random() * recipes.length);
}

const getSearchData = () => recipes.map(recipe => ({ id: recipe.id, title: recipe.title }));
