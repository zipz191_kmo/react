import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles as pageStyles } from '../styles/home';
import { styles } from '../styles/common'
import { useRouter } from '../router';

const RecipeTile = ({ recipe }) => {
  const { id, title, difficulty, time, image } = recipe;
  const router = useRouter();
  return (
    <TouchableOpacity onPress={() => router.toRecipeDetails(id)}>

      <View style={pageStyles.suggestionTile}>
        <Image source={image} style={pageStyles.suggestionTileImage} />
        <View style={[pageStyles.tileInfo, styles.textWhite, pageStyles.suggestionTileInfoContainer]}>
          <Text style={[styles.textWhite, pageStyles.suggestionTileTitle]}>{title}</Text>
          <Text style={[styles.textWhite, pageStyles.suggestionTileSubtext]}>{difficulty}</Text>
          <Text style={[styles.textWhite, pageStyles.suggestionTileSubtext]}>{time}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default RecipeTile;
