import { View, Text } from "react-native";
import { styles as pageStyles } from '../styles/recipeDetails';

export const ItemList = ({ items, ordered = false }) => {
  return (
    <View>
      {items.map((item, index) => (
        <Text key={index} style={pageStyles.listItem}>
          {ordered && (index + 1) + ". "}{item}
        </Text>
      ))}
    </View>
  );
};
