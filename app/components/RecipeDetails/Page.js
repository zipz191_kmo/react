import React, { useState } from 'react';
import { View, Image, Text } from 'react-native';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { styles } from '../styles/common';
import { styles as pageStyles } from '../styles/recipeDetails';
import { TabSelector } from './TabSelector';
import { ItemList } from './ItemList';
import { Texts as T } from '../texts';

const Page = ({ recipe }) => {
  const tabs = {
    Ingredients: "INGREDIENTS",
    Steps: "STEPS",
  }
  const { image, title, description, difficulty, time, ingredients, steps } = recipe;
  const [selectedTab, setSelectedTab] = useState(tabs.Ingredients);

  return (
    <View style={styles.flexContainer}>
      <Image source={{ uri: image }} style={pageStyles.image} />
      <View style={[pageStyles.content, styles.pageContent]}>
        <View style={[pageStyles.header, styles.block]}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>{difficulty}</Text>
        </View>
        <Text style={styles.block}>{description}</Text>
        <View style={[styles.flexRow, styles.block, styles.fontSecondary]}>
          <AccessTimeIcon style={styles.icon} />
          <Text style={styles.fontSecondary}>{time}</Text>
        </View>
        <View style={[styles.flexRow, styles.block, styles.flexSpaceBetween]}>
          <TabSelector key={tabs.Ingredients} title={T.Ingredients} isActive={selectedTab === tabs.Ingredients} onSelect={() => setSelectedTab(tabs.Ingredients)} />
          <TabSelector key={tabs.Steps} title={T.Steps} isActive={selectedTab === tabs.Steps} onSelect={() => setSelectedTab(tabs.Steps)} />
        </View>
        <View style={styles.block}>
          <Text style={[styles.subtitle, styles.block]}>{selectedTab === tabs.Ingredients ? T.Ingredients : T.Steps}</Text>
          <ItemList items={selectedTab === tabs.Ingredients ? ingredients : steps} useOrderNumbers={selectedTab === tabs.Steps} />
        </View>
      </View>
    </View>
  );
};

export default Page;
