import { Text, TouchableOpacity } from 'react-native';
import { styles as pageStyles } from '../styles/recipeDetails';
import { styles } from '../styles/common';

export const TabSelector = ({title, isActive, onSelect }) => {
  return (
    <TouchableOpacity
      style={[pageStyles.tab, isActive && styles.activeTab]}
      onPress={() => onSelect()}>
      <Text style={[styles.tabText, isActive && styles.activeTabText]}>{title}</Text>
    </TouchableOpacity>
  );
}
