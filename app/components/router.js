
import { useRouter as useExpoRouter } from "expo-router";
import { useCallback } from "react";

export const useRouter = () => {
  const router = useExpoRouter();
  const toRecipeDetails = useCallback((id) => router.push(`/recipeDetails/${id}`), []);

  return {
    toRecipeDetails,
  }
}
