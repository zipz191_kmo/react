import { StyleSheet } from "react-native";
import { constants } from "./constants";

export const styles = StyleSheet.create({
  carouselTile: {
    width: 240,
    height: 240,
    margin: 10,
    borderRadius: 10,
    overflow: 'hidden',
  },
  carouselTileImage: {
    width: '100%',
    height: '100%',
  },
  carouselTileTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  carouselTileTitleContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    padding: 10,
    height: 90,
  },
  tileInfo: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  suggestionTileSubtext: {
    fontSize: 14,
    marginTop: 5,
  },
  suggestionTileTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  suggestionTileInfoContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  suggestionTile: {
    width: '100%',
    height: 300,
    marginBottom: 10,
    borderRadius: 10,
    overflow: 'hidden',
  },
  suggestionTileImage: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
  },
  searchBar: {
    height: 40,
    borderWidth: 0,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 10,
    backgroundColor: "#B2DFDB",
  },
  searchSuggestion: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    zIndex: 10000,
    elevation: 10000,
  },
  searchSuggestions: {
    position: 'absolute',
    top: 40,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    zIndex: 10000,
    borderWidth: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderColor: '#ccc',
    elevation: 1000,
  },
  header: {
    backgroundColor: constants.colors.primary,
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    color: "#ffffff",
    zIndex: 10000,
  },
  logo: {
    height: 40,
    width: 100,
    alignSelf: 'center',
  },
  searchBarExpanded: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  }
});
