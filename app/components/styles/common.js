import { StyleSheet } from "react-native";
import { constants } from "./constants";

export const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  block: {
    marginBottom: 10,
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  fontSecondary: {
    color: constants.colors.fontSecondary,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 16,
  },
  flexSpaceBetween: {
    justifyContent: 'space-between',
  },
  activeTab: {
    backgroundColor: '#398378',
  },
  activeTabText: {
    color: "#ffffff",
  },
  tabText: {
    fontSize: 16,
  },
  icon: {
    paddingRight: 10,
    color: "#BDBDBD"
  },
  pageContent: {
    padding: 20,
  },
  section: {
    marginBottom: 30,
    elevation: 0,
  },
  textWhite: {
    color: "#FFFFFF",
  },
  textOverlay: {
    color: '#ffffff',
    fontSize: 18
  }
});
