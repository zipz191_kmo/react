import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 280,
    resizeMode: 'cover',
    marginBottom: 10,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tab: {
    paddingVertical: 10,
    width: 160,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E0F2F1',
    borderRadius: 10,
  },
  content: {
    marginTop: -44,
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  listItem: {
    marginBottom: 8,
    backgroundColor: "#F5F5F5",
    borderRadius: 10,
    padding: 10
  },
});
