import { Stack } from 'expo-router';
import { SafeAreaView } from 'react-native';
import { recipes } from '../../data.json'
import { default as Page } from '../components/RecipeDetails/Page';
import { useSearchParams } from 'expo-router';

const RecipeDetails = () => {
  const params = useSearchParams();
  const { id } = params;

  return (
    <SafeAreaView style={{ backgroundColor: '#ffffff' }}>
      <Stack.Screen options={
        {
          headerShown: false,
        }} />
      {id && <Page recipe={recipes[id - 1]} />}
    </SafeAreaView >)
}

export default RecipeDetails;
