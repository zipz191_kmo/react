import { Stack } from 'expo-router';
import { View, Text, SafeAreaView } from 'react-native';
import { Home } from './components/Home/Page';
import { recipes } from '../data.json'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Index = () => {
  return (
    <SafeAreaView style={{ backgroundColor: '#ffffff' }}>

      <Stack.Screen options={
        {
          headerShown: false,
        }} />
      <Home />
    </SafeAreaView >)
}

export default Index;
